import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    api_url: 'http://127.0.0.1:3000'
  },

  mutations: {},
  
  actions: {
    getMidias(context) {
      return new Promise((resolve, reject) => {
        axios.get(context.state.api_url + '/midias').then(response => {
          resolve(response);
        }).catch(error => reject(error));
      });
    }
  },

  modules: {}
})
